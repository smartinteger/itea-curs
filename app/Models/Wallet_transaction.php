<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet_transaction extends Model
{
    public function wallets()
    {
      return $this->belongsTo('App\Models\Wallet','wallets_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
     // 
  public function currency()
  {
    return $this->belongsTo('App\Models\Currency','currencies_id');
  }
}

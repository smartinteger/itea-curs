<?php
namespace App\Services;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class PrivatApi
{
    private $client;
    private $response;

    //http://docs.guzzlephp.org/en/stable/quickstart.html#sending-requests

    public function __construct()
    {
        // Создать клиента с базовым URI
        $this->client = new \GuzzleHttp\Client(['base_uri' =>'https://api.privatbank.ua/p24api/']);
        
    }

    public function getDataApiCard(){
        try {
            $this->response =  $this->client->request('GET', 'pubinfo?exchange&json&coursid=11');       
        }catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
    }

    public function apiGetBody()
    {
       return $this->response->getBody();
    }

    /**
     * отримати статус відповіді 200-ок
     *  @return void
     */

    public function apiGetStatusCode()
    {
      return $this->response->getStatusCode();
    }

    /**
     * отримати тип контенту    
     *  @return void
     */


    public function apiGetHeaderLine()
    {
       
       // 'application/json; 

        return $this->response->getHeaderLine('content-type');
    }

    /**
     * Ви можете перевірити поточний ліміт швидкості та деталі 
     * використання, ознайомившись із заголовками X-RateLimit-Limit
     *  @return void
     */

    public function apiGetRateLimit()
    {
        return $this->response->getHeaderLine('X-RateLimit-Limit');
    }

    /**
     * повертаються у кожному відповіді API. Наприклад, якщо API 
     * має погодинний ліміт за замовчуванням 1000 запитів, 
     * зробивши 2 запити, ви отримаєте цей HTTP-заголовок у відповіді на другий запит:
     *  @return void
     */

    public function apiGetRateRemaining()
    {
        return $this->response->getHeaderLine('X-RateLimit-Remaining');
    }


}
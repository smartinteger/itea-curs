<?php
namespace App\Traits;

use App\Services\PrivatApi;
use App\Models\Currency;
use App\Models\ExchangeRates;
use Illuminate\Support\Arr;

trait ApiPrivatTrait
{
    private $privatApi;

    /**
     *
     * */
    public function privatApi()
    {
        $this->privatApi = new PrivatApi();
        $this->privatApi->getDataApiCard();
        $request_dada_api = json_decode($this->privatApi->apiGetBody(), true);
        return  $request_dada_api;
    }

    /**
     * с сохранением в базу
     */

    public function privatApiBase()
    {
        $rates = $this->privatApi();
        $currencys = Currency::all()->toArray();

        foreach ($currencys as $currency) {
            $names = Arr::pluck($currencys, 'id');
            $code = Arr::pluck($currencys, 'code');
        }
    
        $new_array_currency = array_combine($code, $names);

        foreach ($rates as $rate) {
            $modelExchangeRates = new ExchangeRates();

            if (isset($new_array_currency[$rate['ccy']]) && isset($new_array_currency[$rate['base_ccy']])) {
                $modelExchangeRates->ccy = $new_array_currency[$rate['ccy']];
                $modelExchangeRates->base_ccy= $new_array_currency[$rate['base_ccy']];
                $modelExchangeRates->buy = $rate['buy'];
                $modelExchangeRates->sale = $rate['sale'];
                $modelExchangeRates->save();
            }
        }
    }
    
    /* array:3 [▼
      0 => array:4 [▼
        "ccy" => 2
        "base_ccy" => 1
        "buy" => "26.45000"
        "sale" => "26.73797"
      ]
      1 => array:4 [▶]
      3 => array:4 [▶]
    ]
     */
    public function privatApiCacheArray()
    {
        if (!\Cache::get('privat_api')) {
            \Cache::remember('privat_api', 900, function () {
                $rates = $this->privatApi();
                $currencys = Currency::all()->toArray();
      
                foreach ($currencys as $currency) {
                    $names = Arr::pluck($currencys, 'id');
                    $code = Arr::pluck($currencys, 'code');
                }
        
                $new_array_currency = array_combine($code, $names);
    
                foreach ($rates as $key=>$rate) {
                    if (isset($new_array_currency[$rate['ccy']]) && isset($new_array_currency[$rate['base_ccy']])) {
                        $array_api_cur[$key]['ccy']=  $new_array_currency[$rate['ccy']];
                        $array_api_cur[$key]['base_ccy'] =  $new_array_currency[$rate['base_ccy']];
                        $array_api_cur[$key]['buy'] =  $rate['buy'];
                        $array_api_cur[$key]['sale'] =  $rate['sale'];
                    }
                }
              
                return $array_api_cur;
            });
        }
       
        return \Cache::get('privat_api');
    }

    public function privatApiCache()
    {
        if (!\Cache::get('privat_api')) {
            \Cache::remember('privat_api', 90, function () {
                return $this->privatApi();
            });
        }
       
        return \Cache::get('privat_api');
    }
}

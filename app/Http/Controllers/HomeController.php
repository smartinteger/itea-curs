<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Currency;
use App\Models\Wallet;
use App\Traits\ApiPrivatTrait;
use App\Models\ExchangeRates;
use Illuminate\Support\Arr;

class HomeController extends Controller
{
    use ApiPrivatTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     
    public function index(Request $request)
    {
        $currency = Currency::all();

        $wallet = Wallet::where('users_id', Auth::user()->id)->get();

        if (Auth::user()->currencies_id==null) {
            \Session::flash('infohtml', 'To display the balance you need to 
            select the main currency of the balance sheet <a data-toggle="modal" 
            data-target="#SaveBaseCurrency" href="#">Save Base Currency</a>');
        }

        foreach ($currency as $currencys) {
            if ($currencys->id == Auth::user()->currencies_id) {
                $user_cur = $currencys['code'];//передадим код валюты пользователя
            }
        }
     

        $balance = 0;
        $usd=0;
        $eur=0;
        $btc=0;
        
        foreach ($this->privatApiCacheArray() as $cache) {
            if ($cache['ccy'] == 2) {
                $usd = $cache['buy'];
            } elseif ($cache['ccy'] == 3) {
                $eur = $cache['buy'];
            } elseif ($cache['ccy'] == 4) {
                $btc = $cache['buy'];
            }
        }
        
        if (Auth::user()->currencies_id == 1) {//если гривна
           
            foreach ($wallet as $wallet_balance) {
                if ($wallet_balance->balance > 0) {
                    if ($wallet_balance->currencies_id == 1) {
                        $balance += $wallet_balance->balance;
                    } else {
                        //если биткоин, то переводим в доллары а потом в гривну
                        if ($wallet_balance->currencies_id == 4) {
                            $balance += ($wallet_balance->balance * $btc) * $usd;
                        } elseif ($wallet_balance->currencies_id == 3) {
                            $balance += $wallet_balance->balance * $eur;
                        } elseif ($wallet_balance->currencies_id == 2) {
                            $balance += $wallet_balance->balance * $usd;
                        }
                    }
                }
            }
        } elseif (Auth::user()->currencies_id == 2) {//если доллар

            foreach ($wallet as $wallet_balance) {
                if ($wallet_balance->balance > 0) {
                    if ($wallet_balance->currencies_id == 2) {
                        $balance += $wallet_balance->balance;
                    } else {
                        //если биткоин
                        if ($wallet_balance->currencies_id == 4) {
                            $balance += ($wallet_balance->balance * $btc);
                        } elseif ($wallet_balance->currencies_id == 3) {
                            $balance += ($wallet_balance->balance * $eur)/$usd;
                        } elseif ($wallet_balance->currencies_id == 1) {
                            $balance += ($wallet_balance->balance / $usd);
                        }
                    }
                }
            }
        } elseif (Auth::user()->currencies_id == 3) { //если евро

            foreach ($wallet as $wallet_balance) {
                if ($wallet_balance->balance > 0) {
                    if ($wallet_balance->currencies_id == 3) {
                        $balance += $wallet_balance->balance;
                    } else {
                        //если биткоин
                        if ($wallet_balance->currencies_id == 4) {
                            $balance += ($wallet_balance->balance * $btc);
                        } elseif ($wallet_balance->currencies_id == 2) {
                            $balance += ($wallet_balance->balance * $usd) / $eur;
                        } elseif ($wallet_balance->currencies_id == 1) {
                            $balance += ($wallet_balance->balance / $eur);
                        }
                    }
                }
            }
        } elseif (Auth::user()->currencies_id == 4) { //если btc

            foreach ($wallet as $wallet_balance) {
                if ($wallet_balance->balance > 0) {
                    if ($wallet_balance->currencies_id == 4) {
                        $balance += $wallet_balance->balance;
                    } else {
                        if ($wallet_balance->currencies_id == 3) {
                            $balance += (($wallet_balance->balance * $eur) / $usd) / $btc;
                        } elseif ($wallet_balance->currencies_id == 2) {
                            $balance += $wallet_balance->balance / $btc;
                        } elseif ($wallet_balance->currencies_id == 1) {
                            $balance += ($wallet_balance->balance / $usd) / $btc;
                        }
                    }
                }
            }
        }


        return view('home', ['currency' => $currency,
                              'wallets'=>$wallet,
                              'balance'=>$balance,
                              'code'=>$user_cur
        ]);
    }
}

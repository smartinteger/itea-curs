<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Currency;

class UserController extends Controller
{
    public function profile()
    {
        $currency = Currency::all();
        $user = User::find(Auth::user()->id);
        return view('user.profile', compact('user', 'currency'));
    }
      

    

    public function update(Request $request)
    {
        if (Auth::check()) {
            $this->validate($request, [
                'name' => 'required',
            ]);
            //https://laravel.com/docs/5.0/validation
            $user = User::find(Auth::user()->id);

            if ($request->password) {
                $user->password = $request->password;
            }

            if ($request->name == $user->name && $user->currencies_id == $request->cyrrency) {
                return back()->with('warning', 'Do you want to change the existing name!');
            }
            $user->name = $request->name;
            $user->currencies_id = $request->cyrrency;
           
            if ($user->save()) {
                $currency = Currency::all();

                return back()->with('success', 'Changes successfully applied!');
            }
            return back()->with('error', 'An error has occurred!');
        }
    }

    public function saveBaseCurrency(Request $request)
    {
        if (Auth::check()) {
            $this->validate($request, [
                'cyrrency' => 'required',
            ]);
            //https://laravel.com/docs/5.0/validation
            $user = User::find(Auth::user()->id);
            $user->currencies_id = $request->cyrrency;
           
            if ($user->save()) {
                $currency = Currency::all();               
                \Session::flash('infohtml', 'You have successfully saved the base currency!');
            } 

            return view('user.profile', compact('user', 'currency'));
        }
    }
}

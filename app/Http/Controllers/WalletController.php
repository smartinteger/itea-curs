<?php

namespace App\Http\Controllers;

use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WalletController extends Controller
{
    const NUMBER_PREFIX_WALLET = 480000000000;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:wallets',
        ]);
        $wallet = new Wallet();
        $wallet->name = $request->name;
        $wallet->number =  (WalletController::NUMBER_PREFIX_WALLET + random_int(0, 9999)) + Auth::user()->id;
        $wallet->currencies_id = $request->cyrrency;
        $wallet->users_id = Auth::user()->id;
        $wallet->balance = 0;
        $wallet->save();       
        //echo "<pre>";dd($wallet->currency->code);

        return view('user.wallet.view')->withWallet($wallet);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $wallet = Wallet::find($request->id);       
        return view('user.wallet.view')->withWallet($wallet);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function edit(Wallet $wallet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $wallet = Wallet::find($request->id);
        $wallet->name = $request->name;              
        $wallet->update();     

        return back()->with('success', 'You have successfully update the wallet number '.$wallet->number);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->id) {
            //$wallet = Wallet::where('id', $request->id)->where('balance',0)->delete();
            $wallet = Wallet::find($request->id);
            $wallet_transaction = \App\Models\Wallet_transaction::where('wallets_id', $wallet->id)->first();

            if ($wallet_transaction) {
                return back()->with('info', 'You cannot delete a wallet with transactions '.$wallet->number);
            } else {
                $wallet->delete();
                return back()->with('success', 'You have successfully deleted the wallet number! '.$wallet->number);
            }


         /*    if ($wallet->balance>0) {
                return back()->with('info', 'You cannot delete a wallet with funds '.$wallet->number);
            } else {
                $wallet->delete();
                return back()->with('success', 'You have successfully deleted the wallet number! '.$wallet->number);
            } */
        }
    }
}

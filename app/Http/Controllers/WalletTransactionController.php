<?php

namespace App\Http\Controllers;

use App\Models\Wallet_transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Wallet;
use App\Models\Currency;
use App\Traits\ApiPrivatTrait;

class WalletTransactionController extends Controller
{

    use ApiPrivatTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currency = Currency::all();
        $wallet = Wallet::where('users_id', Auth::user()->id)->get();

        foreach ($currency as $currencys) {
            if ($currencys->id == Auth::user()->currencies_id) {
                $user_cur = $currencys['code'];//передадим код валюты пользователя
            }
        }
     

        $balance = 0;
        $usd=0;
        $eur=0;
        $btc=0;
      
        
        foreach ($this->privatApiCacheArray() as $cache) {
            if ($cache['ccy'] == 2) {
                $usd = $cache['buy'];
            } elseif ($cache['ccy'] == 3) {
                $eur = $cache['buy'];
            } elseif ($cache['ccy'] == 4) {
                $btc = $cache['buy'];
            }
        }
        
        if (Auth::user()->currencies_id == 1) {//если гривна
           
            foreach ($wallet as $wallet_balance) {
                if ($wallet_balance->balance > 0) {
                    if ($wallet_balance->currencies_id == 1) {
                        $balance += $wallet_balance->balance;
                    } else {
                        //если биткоин, то переводим в доллары а потом в гривну
                        if ($wallet_balance->currencies_id == 4) {
                            $balance += ($wallet_balance->balance * $btc) * $usd;
                        } elseif ($wallet_balance->currencies_id == 3) {
                            $balance += $wallet_balance->balance * $eur;
                        } elseif ($wallet_balance->currencies_id == 2) {
                            $balance += $wallet_balance->balance * $usd;
                        }
                    }
                }
            }
        } elseif (Auth::user()->currencies_id == 2) {//если доллар

            foreach ($wallet as $wallet_balance) {
                if ($wallet_balance->balance > 0) {
                    if ($wallet_balance->currencies_id == 2) {
                        $balance += $wallet_balance->balance;
                    } else {
                        //если биткоин
                        if ($wallet_balance->currencies_id == 4) {
                            $balance += ($wallet_balance->balance * $btc);
                        } elseif ($wallet_balance->currencies_id == 3) {
                            $balance += ($wallet_balance->balance * $eur)/$usd;
                        } elseif ($wallet_balance->currencies_id == 1) {
                            $balance += ($wallet_balance->balance / $usd);
                        }
                    }
                }
            }
        } elseif (Auth::user()->currencies_id == 3) { //если евро

            foreach ($wallet as $wallet_balance) {
                if ($wallet_balance->balance > 0) {
                    if ($wallet_balance->currencies_id == 3) {
                        $balance += $wallet_balance->balance;
                    } else {
                        //если биткоин
                        if ($wallet_balance->currencies_id == 4) {
                            $balance += ($wallet_balance->balance * $btc);
                        } elseif ($wallet_balance->currencies_id == 2) {
                            $balance += ($wallet_balance->balance * $usd) / $eur;
                        } elseif ($wallet_balance->currencies_id == 1) {
                            $balance += ($wallet_balance->balance / $eur);
                        }
                    }
                }
            }
        } elseif (Auth::user()->currencies_id == 4) { //если btc

            foreach ($wallet as $wallet_balance) {
                if ($wallet_balance->balance > 0) {
                    if ($wallet_balance->currencies_id == 4) {
                        $balance += $wallet_balance->balance;
                    } else {
                        if ($wallet_balance->currencies_id == 3) {
                            $balance += (($wallet_balance->balance * $eur) / $usd) / $btc;
                        } elseif ($wallet_balance->currencies_id == 2) {
                            $balance += $wallet_balance->balance / $btc;
                        } elseif ($wallet_balance->currencies_id == 1) {
                            $balance += ($wallet_balance->balance / $usd) / $btc;
                        }
                    }
                }
            }

           
        } 
        $wallet_transaction = Wallet_transaction::where('users_id', Auth::user()->id)->get();
            
            return view('user.transaction.transaction', [
                              'wallets_transaction'=>$wallet_transaction,
                              'balance'=>$balance,
                              'code'=>$user_cur,
                           
                              'usd'=> $usd,
                              'eur'=> $eur,
                              'btc'=> $btc]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $wallets = Wallet::where('users_id', Auth::user()->id)->get();

        return view('user.transaction.create')->withWallets($wallets);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$request->flash();
        $this->validate($request, [
        'transaction_description' => 'required|unique:wallet_transactions|max:300',
        'summ' => 'required',
        ]);

        $wallet_transaction = new Wallet_transaction();
        $wallet = Wallet::where([['users_id' ,'=', Auth::user()->id],['id','=', $request->wallet]])->first();

        if ($request->transaction == 'payment') {
            if ($wallet->balance >= $request->summ) {
                $wallet->balance = $wallet->balance - $request->summ;
            } else {
                return back()->with('info', 'wallet balance'.$wallet->balance. ' less than transaction amount ' . $request->summ);
            }
        }

        if ($request->transaction == 'receipt') {
            $wallet->balance = $wallet->balance + $request->summ;
        }


        if ($wallet->update()) {
            $wallet_transaction->type_transaction = $request->transaction;
            $wallet_transaction->users_id = $wallet->users_id;
            $wallet_transaction->wallets_id = $wallet->id;
            $wallet_transaction->currencies_id = $wallet->currencies_id;
            $wallet_transaction->summ = $request->summ;
            $wallet_transaction->transaction_description = $request->transaction_description;
            $wallet_transaction->save();
        }
       
      
        return redirect(app()->getLocale().'/transaction');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Wallet_transaction  $wallet_transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Wallet_transaction $wallet_transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Wallet_transaction  $wallet_transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Wallet_transaction $wallet_transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Wallet_transaction  $wallet_transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    
        if ($request->method()=="GET") {
            $wallets_transaction = Wallet_transaction::find($request->id);
          
            $wallets = Wallet::where('users_id', Auth::user()->id)->get();

            return view('user.transaction.update')->withWalletsTransaction($wallets_transaction)->withWallets($wallets);
        } 
//post
        $this->validate($request, [
            'description' => 'required|max:300',
            'summ' => 'required',
        ]);

        $wallet_transaction = Wallet_transaction::find($request->id);
        $wallet = Wallet::where([['users_id' ,'=', Auth::user()->id],['id','=', $request->wallet]])->first();

        if ($request->transaction == 'payment') {
            if ($wallet->balance >= $request->summ) {
                $wallet->balance = $wallet->balance - $request->summ;
            } else {
                return back()->with('info', 'wallet balance'.$wallet->balance. ' less than transaction amount ' . $request->summ);
            }
        }

        if ($request->transaction == 'receipt') {
            $wallet->balance = $wallet->balance + $request->summ;
        }


        if ($wallet->update()) {
            $wallet_transaction->type_transaction = $request->transaction;
            $wallet_transaction->users_id = $wallet->users_id;
            $wallet_transaction->wallets_id = $wallet->id;
            $wallet_transaction->currencies_id = $wallet->currencies_id;
            $wallet_transaction->summ = $request->summ;
            $wallet_transaction->transaction_description = $request->description;
            $wallet_transaction->update();
        }

              
        return redirect(app()->getLocale().'/transaction');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Wallet_transaction  $wallet_transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->id) {
            //$wallet = Wallet::where('id', $request->id)->where('balance',0)->delete();
            $wallet_transaction = Wallet_transaction ::find($request->id);

            $wallet_transaction->delete();
            return back()->with('success', 'You have successfully deleted the wallet transaction number! '.$wallet_transaction->id);
        }
    }
}

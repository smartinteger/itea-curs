<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect(app()->getLocale());
});

Route::group(['prefix' => '{locale}',
  'where' => ['locale' => '[a-zA-Z]{2}'],
   'middleware' => 'setlocale'
], function () {
    Route::get('/', function () {
        return view('welcome');
    });

    Auth::routes();


    Route::group(['middleware' => ['web']], function () {
        Route::group(['middleware' => ['auth']], function () {
            Route::get('/home', 'HomeController@index')->name('home');
            //----User profile----
            Route::get('/profile', ['uses'=>'UserController@profile', 'as'=>'users.profile']);
            // Route::get('/profile/edit', ['uses'=>'UserController@edit', 'as'=>'users.edit']);
            Route::post('/profile/edit', ['uses'=>'UserController@update', 'as'=>'users.edit']);
            Route::post('/profile/save-base-currency', ['uses'=>'UserController@saveBaseCurrency', 'as'=>'users.saveBaseCurrency']);
            //--------------------
            Route::get('/wallets', 'WalletController@index')->name('wallets');
            Route::post('/wallets/create', 'WalletController@create')->name('wallets.create');
            Route::post('/wallets/update', 'WalletController@update')->name('wallets.update');
            Route::get('/wallets/show/{id}', 'WalletController@show')->where('id', '[0-9]+')->name('wallets.show');
            Route::get('/wallets/destroy/{id}', 'WalletController@destroy')->where('id', '[0-9]+')->name('wallets.destroy');
            //-------------------
           // Route::resource('transaction', 'WalletTransactionController');
         
           Route::get('/transaction', 'WalletTransactionController@index')->name('transaction');
            Route::get('/transaction/create', 'WalletTransactionController@create')->name('transaction.create');
            Route::get('/transaction/destroy/{id}', 'WalletTransactionController@destroy')->where('id', '[0-9]+')->name('transaction.destroy');
            Route::post('/transaction/store', 'WalletTransactionController@store')->name('transaction.store');
         Route::get('/transaction/update/{id}', 'WalletTransactionController@update')->where('id', '[0-9]+')->name('transaction.update');
    Route::post('/transaction/update', 'WalletTransactionController@update')->name('transaction.update');
        
            //Route::view('/transaction/create/form', 'user.transaction.create')->name('user.transaction.create.form');



        });
    });
});

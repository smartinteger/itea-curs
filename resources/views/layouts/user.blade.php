<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>



  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<!------ Include the above in your HEAD tag ---------->

<body>

  <div id="wrapper" class="animate">
    <nav class="navbar header-top fixed-top navbar-expand-lg navbar-dark bg-dark">
      <span class="navbar-toggler-icon leftmenutrigger"></span>
      <a class="navbar-brand" href="{{ url(app()->getLocale() .'/home') }}">WALLET</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
        aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav animate side-nav">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-user"></i> Submenu <i class="fas fa-user shortmenu animate"></i>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{ url(app()->getLocale() .'/home') }}">Home</a>
              <a class="dropdown-item" href="{{ url(app()->getLocale() .'/transaction') }}">Create Transaction</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="{{ url(app()->getLocale() .'/profile') }}">Profile</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url(app()->getLocale() .'/transaction') }}" title="Cart"><i class="fas fa-cart-plus"></i> Transaction <i
                class="fas fa-cart-plus shortmenu animate"></i></a>
          </li>
         
        </ul>
        <ul class="navbar-nav ml-md-auto d-md-flex">
          <!-- local -->
          <!-- local -->
          @foreach (config('app.available_locales') as $locale)
          <li class="nav-item">
              <a class="nav-link" href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), $locale) }}" @if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline"
                  @endif>{{ strtoupper($locale) }}</a>
          </li>
          @endforeach
          <li class="nav-item">
          <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false" v-pre>
              <i class="fas fa-user"></i>
              {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{ url(app()->getLocale() .'/transaction') }}">Платежи</a>
              <a class="dropdown-item" href="{{ url(app()->getLocale() .'/home') }}">Кошельки</a>
              <a class="dropdown-item" href="{{ url(app()->getLocale() .'/profile') }}">Профиль</a>
              

              <a class="dropdown-item" href="{{ route('logout', app()->getLocale()) }}" onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
              </a>


              <form id="logout-form" action="{{ route('logout', app()->getLocale()) }}" method="POST"
                style="display: none;">
                @csrf
              </form>
            </div>
          </li>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout', app()->getLocale()) }}" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
              <i class="fas fa-key"></i>
              {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout', app()->getLocale()) }}" method="POST"
              style="display: none;">
              @csrf
            </form>

          </li>



        </ul>
      </div>
    </nav>
    <div class="container-fluid">
      
      <main class="py-4">
        @include('flash-message')

        @yield('content')
      </main>
    </div>
  </div>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
    crossorigin="anonymous"></script>
  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</body>

</html>
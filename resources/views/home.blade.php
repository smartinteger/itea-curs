@extends('layouts.user') @section('content')
<h5>Balance {{$balance." ". $code}} </h5>
<div class="row">
    <div class="col">
        <div class="card">

            <button href="" class="m-5 btn btn-success col-2" data-toggle="modal" data-target="#walletcreate">
        Create Wallet
      </button>

            <div class="card-body">
                <h5 class="card-title">Wallet List</h5>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Number</th>
                            <th scope="col">Currency</th>
                            <th scope="col">Balance</th>
                            <th scope="col">Manage</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @if(count($wallets)>0) @foreach ($wallets as $num =>$wallet)

                        <tr>
                            <th scope="row">{{$num+1}}</th>
                            <td>{{$wallet->name}}</td>
                            <td>{{$wallet->number}}</td>
                            <td>{{$wallet->currency->code}}</td>
                            <td>{{$wallet->balance}}</td>
                            <td>
                                <a data-toggle="modal" data-target="#walletupdate" href=""><i class="fas fa-edit"></i></a>
                                <a href="{{ url(app()->getLocale().'/wallets/destroy/'.$wallet->id)}}"><i
                    class="far fa-trash-alt"></i></a>
                            </td>
                        </tr>
                        @endforeach @else
                        <tr>
                            <th>Create Wallet</th>
                        </tr>
                        @endif


                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- Modal create-->
    <div class="modal fade" id="walletcreate" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Wallet create</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('wallets.create', app()->getLocale()) }}" method="post">
                        @csrf

                        <div class="form-group">
                            <label for="name">Name wallet</label>
                            <input type="text" name="name" class="form-control" id="">
                        </div>

                        <div class="form-group">
                            <label for="currency">Currency</label>
                            <select name='cyrrency' class="form-control">
                @foreach ($currency as $item)
                <option value="{{$item->id}}">{{$item->code}}</option>
                @endforeach
              </select>
                        </div>

                        <input type="submit" name='publish' class="btn btn-success" value="Create" />
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </form>
                </div>
                <div class="modal-footer">
                    Create Wallett
                </div>
            </div>
        </div>
    </div>

    <!-- Modal update-->
    <div class="modal fade" id="walletupdate" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Wallet update</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
                </div>

                <div class="modal-body">
                    <form action="{{ route('wallets.update', app()->getLocale()) }}" method="post">
                        @csrf @if(isset($wallet))

                        <div class="form-group">
                            <label for="name">Name wallet</label>
                            <input type="text" name="name" value="{{$wallet->name}}" class="form-control" id="">
                        </div>

                        <input type="hidden" value="{{$wallet->id}}" name="id">

                @endif
                        <input type="submit" name='publish' class="btn btn-success" value="Update" />
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </form>
                </div>
                <div class="modal-footer">
                    Update Wallett
                </div>
            </div>
        </div>
    </div>


    <!-- Modal update-->
    <div class="modal fade" id="SaveBaseCurrency" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Save Base Currency</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
                </div>

                <div class="modal-body">
                    <form action="{{ route('users.saveBaseCurrency', app()->getLocale()) }}" method="post">
                        @csrf


                        <div class="form-group">
                            <label for="currency">Currency</label>
                            <select name='cyrrency' class="form-control">
                @foreach ($currency as $item)
                <option value="{{$item->id}}">{{$item->code}}</option>
                @endforeach
              </select>
                        </div>

                        <input type="submit" name='publish' class="btn btn-success" value="Save" />
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </form>
                </div>
                <div class="modal-footer">
                    Save Base Currency
                </div>
            </div>
        </div>
    </div>

    @endsection
@extends('layouts.user')

@section('content')
<div class="row">
    <div class="col">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">New Wallet Create</h5>
          <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Number</th>
                <th scope="col">Currency</th>
                <th scope="col">Balance</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td>{{$wallet->name}}</td>
                <td>{{$wallet->number}}</td>
                <td>{{$wallet->currency->code}}</td>
                <td>{{$wallet->balance}}</td>
              </tr>
             
            </tbody>
          </table>
        </div>
      </div>
    </div> 
@endsection
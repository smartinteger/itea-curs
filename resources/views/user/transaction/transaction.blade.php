@extends('layouts.user')

@section('content')
<h5>Balance {{$balance." ". $code}} </h5>
      <div class="row">
        <div class="col">
          <div class="card">

            <a href="{{ route('transaction.create', app()->getLocale())}}" class="m-5 btn btn-success col-2">
              Create Transaction
            </a>

            <div class="card-body">
              <h5 class="card-title">Wallet transaction List</h5>
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Date</th>
                    <th scope="col">Type</th>
                    <th scope="col">Wallet</th>
                    <th scope="col">Summ</th>
                    <th scope="col">Description</th>
                    <th scope="col">Manage</th>
                  </tr>
                </thead>
                <tbody>
         @if(count($wallets_transaction)>0)
                  @foreach ($wallets_transaction as $num =>$wallet_transaction)                
                  
                  <tr>
                      
                    <th scope="row">{{$num+1}}</th>
                    <td>{{$wallet_transaction->created_at}}</td>
                    <td>{{$wallet_transaction->type_transaction}}</td>
                    <td>{{$wallet_transaction->wallets->name}}</td>
                    <td>{{$wallet_transaction->summ}}</td>
                    <td>


                      @if(Auth::user()->currencies_id == 1)
                            @if ($wallet_transaction->currencies_id == 1) 
                                  {{$wallet_transaction->summ}}
                            @else 
                                 @if ($wallet_transaction->currencies_id == 4) 
                                     {{($wallet_transaction->summ * $btc) * $usd}}
                                @elseif ($wallet_transaction->currencies_id == 3)
                                   {{ $wallet_transaction->summ * $eur}}
                             @elseif ($wallet_transaction->currencies_id == 2) 
                              {{$balance += $wallet_transaction->summ * $usd}}
                             @endif
                           @endif
                      @endif

                      @if(Auth::user()->currencies_id == 2)
                           @if ($wallet_transaction->currencies_id == 2) 
                               {{$wallet_transaction->summ}}
                           @else 
                              @if ($wallet_transaction->currencies_id == 4) 
                                   {{$wallet_transaction->summ * $btc}}
                              @elseif ($wallet_transaction->currencies_id == 3)
                                   {{ ($wallet_transaction->summ * $eur)/$usd}}
                              @elseif ($wallet_transaction->currencies_id == 1) 
                                   {{$wallet_transaction->summ / $usd}}
                          @endif
                        @endif
                      @endif   
                      
                      
                      @if(Auth::user()->currencies_id == 3)
                           @if ($wallet_transaction->currencies_id == 3) 
                               {{$wallet_transaction->summ}}
                           @else 
                              @if ($wallet_transaction->currencies_id == 4) 
                                   {{$wallet_transaction->summ * $btc}}
                              @elseif ($wallet_transaction->currencies_id == 2)
                                   {{ ($wallet_transaction->summ * $usd) / $eur}}
                              @elseif ($wallet_transaction->currencies_id == 1) 
                                   {{$wallet_transaction->summ / $eur}}
                          @endif
                        @endif
                      @endif   

                      @if(Auth::user()->currencies_id == 4)
                      @if ($wallet_transaction->currencies_id == 4) 
                          {{$wallet_transaction->summ}}
                      @else 
                         @if ($wallet_transaction->currencies_id == 3) 
                              {{(($wallet_transaction->summ * $eur) / $usd) / $btc}}
                         @elseif ($wallet_transaction->currencies_id == 2)
                              {{ $wallet_transaction->summ / $btc}}
                         @elseif ($wallet_transaction->currencies_id == 1) 
                              {{($wallet_transaction->summ / $usd) / $btc}}
                     @endif
                   @endif
                 @endif   

                     </td>
                    <td>{{$wallet_transaction->transaction_description}}</td>
                    <td>   
                       <a href="{{ url(app()->getLocale().'/transaction/update/'.$wallet_transaction->id)}}"><i class="fas fa-edit" ></i></a>
                       <a href="{{ url(app()->getLocale().'/transaction/destroy/'.$wallet_transaction->id)}}"><i class="far fa-trash-alt"></i></a>                    
                    </td>
                  </tr>
                  @endforeach
       @else
       <tr><th>Create Wallet transaction</th></tr>
       @endif
            
      
                </tbody>
              </table>
            </div>
          </div>
        </div> 





@endsection

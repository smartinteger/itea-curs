@extends('layouts.user') 
@section('content')
<div class="row p-4 col-7">
    <div class="col">
        <div class="card p-4">

            <form action="{{ route('transaction.store', app()->getLocale()) }}" method="post">
                @csrf

                <div class="form-group row">
                    <label for="transaction_description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>
                    <div class="col-md-6">
                        <input class="@error('transaction_description') is-invalid @enderror form-control" type="text" value="{{ old('transaction_description') }}" name="transaction_description" id=""> 
                        @error('transaction_description')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                        </span>
                     @enderror
                    </div>

                </div>

                <div class="form-group row">
                    <label for="transaction" class="col-md-4 col-form-label text-md-right">{{ __('Transaction') }}</label>
                    <div class="col-md-6">
                        <select name='transaction' class="form-control">
                            <option value="receipt">Приход</option>
                            <option value="payment">Расход</option>
                        </select>

                    </div>
                </div>

                <div class="form-group row">
                    <label for="transaction" class="col-md-4 col-form-label text-md-right">{{ __('Wallet') }}</label>
                    <div class="col-md-6">

                        <select name='wallet' class="form-control">
                            @foreach ($wallets as $item)
                            <option value="{{$item->id}}">{{$item->name." ".$item->number}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="summ" class="col-md-4 col-form-label text-md-right">{{ __('Summ') }}</label>
                    <div class="col-md-6">
                        <input class="@error('summ') is-invalid @enderror form-control" type="text" value="{{ old('summ') }}" name="summ" id="">
                        @error('summ')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <input type="submit" class="btn btn-success" value="Create" />
                        <a href="{{ route('transaction.index', app()->getLocale()) }}" class="btn btn-secondary">{{__('Close')}}</a>
                     </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@extends('layouts.user')
 @section('content')

<h5>Update wallet transaction {{$walletsTransaction->id}}</h5>

<div class="row p-4 col-7">

    <div class="col">
        <div class="card p-4">

            <form action="{{ route('transaction.update', app()->getLocale()) }}" method="post">
                @csrf
                <input type="hidden" value="{{ $walletsTransaction->id}}" name="id">
                <div class="form-group row">
                    <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>
                    <div class="col-md-6">
                        <input class="@error('description') is-invalid @enderror form-control" type="text" value="{{ old('description', $walletsTransaction->transaction_description) }}" name="description" id=""> 
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                        </span>
                     @enderror
                    </div>

                </div>

                <div class="form-group row">
                    <label for="transaction" class="col-md-4 col-form-label text-md-right">{{ __('Transaction') }}</label>
                    <div class="col-md-6">
                        <select name='transaction' class="form-control">  

                            @if("receipt" == $walletsTransaction->type_transaction)
                              <option selectedvalue="receipt" value="receipt">Приход</option>
                              <option value="payment">Расход</option>
                              @else
                              <option  value="receipt">Приход</option>
                              <option selected value="payment">Расход</option>
                            @endif

                          
                        </select>

                    </div>
                </div>

                <div class="form-group row">
                    <label for="transaction" class="col-md-4 col-form-label text-md-right">{{ __('Wallet') }}</label>
                    <div class="col-md-6">

                        <select name='wallet' class="form-control">
                            @foreach ($wallets as $item)                        
                            @if($item->id == $walletsTransaction->wallets_id)
                                  <option selected value="{{$item->id}}">{{$item->name." ".$item->number}}</option>
                           @else
                           <option value="{{$item->id}}">{{$item->name." ".$item->number}}</option>
                                  @endif
                            
                          

                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="summ" class="col-md-4 col-form-label text-md-right">{{ __('Summ') }}</label>
                    <div class="col-md-6">

                        <input class="@error('summ') is-invalid @enderror form-control" type="text" value="{{ old('summ', $walletsTransaction->summ) }}" name="summ" id="">
                      
                        @error('summ')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <input type="submit"  class="btn btn-success" value="Update" />
                        <a href="{{ route('transaction', app()->getLocale()) }}" class="btn btn-secondary">{{__('Close')}}</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
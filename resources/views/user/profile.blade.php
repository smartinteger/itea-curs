@extends('layouts.user')
@section('title')
{{ Auth::user()->name }}
@endsection
@section('content')
<div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
  <ul class="list-group">

    <li class="list-group-item">
      Профиль создан {{Auth::user()->created_at->format('M d,Y \a\t h:i a') }}
    </li>

    <li class="list-group-item panel-body">

      <form action = "{{ route('users.edit', app()->getLocale()) }}" method="post">
        @csrf

    <li class="list-group-item">
      <div class="row">
        <div class="col-md-2 p-2 float-center">
         Name
        </div>

        <div class="col-md-5 float-left">
          <input type="text" name="name" value="{{$user->name}}" class="form-control" id="">
        </div>
      </div>
    </li>

    <li class="list-group-item">
      <div class="row">
        <div class="col-md-2 p-2 float-center">
          New Password
        </div>

        <div class="col-md-5 float-left">
          <div class="form-group">
            <div class="input-group" id="show_hide_password">
              <input type="password" name="password" value="" class="form-control" id="show_hide_password">

              <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2"><a href=""><i class="fa fa-eye-slash"
                      aria-hidden="true"></i></a></span>
              </div>
            </div>
          </div>


        </div>

      </div>
    </li>

    <li class="list-group-item">
      <div class="row">
        <div class="col-md-2 p-2 float-center">
          Currency
        </div>

        <div class="col-md-5 float-left">
          <select name='cyrrency' class="form-control">
            @foreach ($currency as $item)
            @if($item->id === $user->currencies_id)
            <option selected value="{{$item->id}}">{{$item->code}}</option>
            @else
            <option value="{{$item->id}}">{{$item->code}}</option>
            @endif
            @endforeach
          </select>
        </div>

      </div>
    </li>


    <li class="list-group-item">
      <div class="row">
        <div class="col-md-2 p-2 float-center">
          Mail
        </div>
        <div class="col-md-5 float-left">


          <div class="form-group">
            <div class="input-group">
              <input type="text" disabled name="base_currency" value="{{$user->email}}" class="form-control" id="">

              <div class="input-group-append">
                <span class="input-group-text" id="basic-addon1">@</span>
              </div>
            </div>
          </div>
        </div>
    </li>
    <li class="list-group-item">
      <input type="submit" name='publish' class="btn btn-success" value="Update" />
    </li>
  </ul>

  </form>

</div>

@endsection